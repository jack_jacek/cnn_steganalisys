import os
from random import randint
import hashlib
import json

def stego(i, folder):
    steghide = "G:/Steghide/steghide embed "
    image_org_path = "H:/ITB/Stego/payload/Test_Final_Payload_256x256/"
    image_stego_path = "H:/ITB/Stego/payload/StegHide/"+str(folder)+"/"
    image_secret_path = "H:/ITB/Stego/payload/"+str(folder)+"/"

    data = {}
    data['stego'] = []

    for filename in os.listdir(image_org_path):
        image_org_file_name = "image_test_0"+str(i)+".jpg"
        image_stego_file_name = "s_image_test_0"+str(i)+".jpg"
        image_secret_file_name = "secret0"+str(i)+".txt"

        image_org_file_name = "image_test_0"+str(i)+".jpg"
        image_stego_file_name = "s_images_0"+str(i)+".jpg"
        image_secret_file_name = "secret0"+str(i)+".txt"
        pin = pin_generator(5)

        print("!",steghide + "-cf "+ image_org_path + image_org_file_name
                    + " -ef " + image_secret_path + image_secret_file_name
                    + " -p " + pin
                    + " -sf " + image_stego_path + image_stego_file_name)

        output = os.popen(steghide + "-cf "+ image_org_path + image_org_file_name
                    + " -ef " + image_secret_path + image_secret_file_name
                    + " -p " + pin
                    + " -sf " + image_stego_path + image_stego_file_name).read()
        print("Output:", output)
        #logging
        if (output.find("embedding")==1):
            data['stego'].append('{ "image_org_file_name":'+image_org_file_name+','
                             ' "image_org_file_hash":'+md5(image_org_path + image_org_file_name)+','
            
                             ' "image_stego_file_name":'+image_stego_file_name+','
                             ' "image_stego_file_hash":'+md5(image_stego_path + image_stego_file_name)+','  
            
                             ' "image_secret_file_name":'+image_secret_file_name+','
                             ' "pin":'+ pin+' }')
        i+=1
        save(data['stego'])

def pin_generator(length):
    pin = ""
    for x in range(length):
        value = randint(48, 57)
        pin+= chr(value)
    return pin

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def save(jason_data):
    with open('stego_logs_1.txt', 'w') as outfile:
        json.dump(jason_data, outfile)

if __name__ == "__main__":
    name = 0
    for z in range(19):
        name = name + 5
        stego(1, name)

