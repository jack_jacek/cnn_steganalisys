import os
import hashlib

myList = []
path_name = "G:/ITB_Images/Test_Final256x256/"
jpg_name = "newimage"

def md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for small_chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(small_chunk)
    return hash_md5.hexdigest()

def dublication():
    for filename in os.listdir(path_name):
        hash = md5(path_name+filename)
        if hash in myList:
            print ("FOUND: ", filename)
            os.remove(path_name+ filename)
        else:
            myList.append(hash)

if __name__ == "__main__":
    dublication()
