import os

import PIL
from PIL import Image

path_from = "G:/ITB_Images/Test_Final/"
path_to = "G:/ITB_Images/Test_Final256x256/"


def crop():
    for filename in os.listdir(path_from):
        img = Image.open(path_from+filename)
        img = img.resize((256, 256), PIL.Image.ANTIALIAS)
        img.save(path_to+filename)
        os.remove(path_from+filename)

if __name__ == "__main__":
    crop()
