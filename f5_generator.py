import os
from random import randint
import hashlib
import json

steghide = "java -jar G:/f5/f5.jar "
image_org_path = "G:/ITB_Images/test256x256/"
image_secret_path = "H:/ITB/Stego/secret_1/"
image_stego_path = "G:/ITB_Images/test_steg256x256/"

image_org_path = "G:/ITB_Images/Test_Final256x256/"
image_stego_path = "G:/ITB_Images/Test_Final_F5_256x256/"

data = {}
data['stego'] = []



def stego(i):
    for filename in os.listdir(image_org_path):
        image_org_file_name = "image_test_0"+str(i)+".jpg"
        image_stego_file_name = "s_image_test_0"+str(i)+".jpg"
        image_secret_file_name = "secret0"+str(i)+".txt"

        image_org_file_name = "image_test_0"+str(i)+".jpg"
        image_stego_file_name = "s_images_0"+str(i)+".jpg"
        image_secret_file_name = "secret0"+str(i)+".txt"
        pin = pin_generator(5)

        text = (steghide + "-e " + image_secret_path + image_secret_file_name
              + " -p " + pin
              + " -q 100"
              + image_org_path + image_org_file_name
              + image_stego_path + image_stego_file_name)

        print(text)

        output = os.popen(steghide + " e -e " + image_secret_path + image_secret_file_name
              + " -p " + pin
              + " -q 90"
              + " " + image_org_path + image_org_file_name
              + " " + image_stego_path + image_stego_file_name)

        print("Output:", output)


        i+=1


def pin_generator(length):
    pin = ""
    for x in range(length):
        value = randint(48, 57)
        pin+= chr(value)
    return pin

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def save(jason_data):
    with open('stego_logs_1.txt', 'w') as outfile:
        json.dump(jason_data, outfile)

if __name__ == "__main__":
    stego(524)
    #stego(30010)
