import json
from random import randint

data = {}
data['people'] = []
data['people'].append('{ "name":"Chaya", "age":12, "city":"Boulder", "type":"Canine" }')
data['people'].append('{ "name":"Tom", "age":32, "city":"Ashbourne", "type":"Blaaa" }')

json_sample = json.loads(data['people'][0])
print(json_sample["name"])

with open('data.txt', 'w') as outfile:
     json.dump(data, outfile)


