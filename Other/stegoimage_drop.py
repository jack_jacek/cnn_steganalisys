# baseline model for the dogs vs cats dataset
import json
import sys

import tensorflow as tf
from matplotlib import pyplot
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D, Dropout
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.optimizers import SGD, Adam
from keras.preprocessing.image import ImageDataGenerator
import pandas as pd

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=config)

# define cnn model
def define_model(_drop):
    model = Sequential()
    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same',
                     input_shape=(256, 256, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(_drop))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(1, activation='sigmoid'))

    # compile model
    opt = SGD(lr=0.00055, momentum=0.9)
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])

    return model


# plot diagnostic learning curves
def summarize_diagnostics(history, x):
    # convert the history.history dict to a pandas DataFrame:
    hist_df = pd.DataFrame(history.history)

    # or save to csv:
    hist_csv_file = 'csv/history_'+str(x)+'.csv'
    with open(hist_csv_file, mode='w') as f:
        hist_df.to_csv(f)

    # plot loss
    pyplot.subplot(211)
    pyplot.title('Cross Entropy Loss')
    pyplot.plot(history.history['loss'], color='blue', label='train')
    pyplot.plot(history.history['val_loss'], color='orange', label='test')
    # plot accuracy
    pyplot.subplot(212)
    pyplot.title('Classification Accuracy')
    pyplot.plot(history.history['accuracy'], color='blue', label='train')
    pyplot.plot(history.history['val_accuracy'], color='orange', label='test')

    # save plot to file
    pyplot.savefig('output/plot_0'+str(x)+'.png')
    pyplot.close()


# run the test harness for evaluating a model
def run_test_harness(x, _dropout):
    # define model
    model = define_model(_dropout)
    # create data generator
    datagen = ImageDataGenerator(rescale=1.0 / 255.0)
    # prepare iterators
    train_it = datagen.flow_from_directory('data/train/',
                                           class_mode='binary', batch_size=32, target_size=(256, 256))
    test_it = datagen.flow_from_directory('data/test/',
                                          class_mode='binary', batch_size=32, target_size=(256, 256))
    # fit model
    history = model.fit_generator(train_it, steps_per_epoch=len(train_it),
                                  validation_data=test_it, validation_steps=len(test_it), epochs=50, verbose=0)
    # evaluate the model
    loss, acc = model.evaluate(test_it, verbose=0)
    print('Accuracy: %.3f' % acc)
    print('Loss: %.3f' % loss)

    print('> %.3f' % (acc * 100.0))
    # learning curves

    summarize_diagnostics(history, x)
    #model.save('final_model_stego_new5.h5')

    return acc, loss

# entry point, run the test
_dropout = 0.6

acc_list = []
loss_list = []

for x in range(6, 10):
    print('Test No: ', 1+x)
    print('Dropout: ', _dropout)
    acc, loss = run_test_harness(x, _dropout)
    acc_list.append(acc)
    loss_list.append(loss)
    _dropout+= 0.1


print('Progress (Acc)  : ', acc_list)
print('Progress: (Loss):', loss_list)