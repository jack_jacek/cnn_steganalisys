# make a prediction for a new image.
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from PIL import Image

# load and prepare the image
def load_image(filename):
    # load the image
    img = load_img(filename, target_size=(256, 256))
    # convert to array
    img = img_to_array(img)
    # reshape into a single sample with 3 channels
    img = img.reshape(1, 256, 256, 3)
    # center pixel data
    img = img.astype('float32')
    #img = img - [123.68, 116.779, 103.939]
    return img


# load an image and predict the class
def run_example():
    import glob

    model = load_model('epp500-final.h5')
    print(model.summary())
    txtfiles = []
    for file in glob.glob("data/test/stego/*.jpg"):
    #for file in glob.glob("data/test/stego/*.jpg"):

        img = load_image(file)
        # load model
        # predict the class
        #result = model.predict(img)

        #print(result,file)

        #print("new")
        y_prob = model.predict_classes(img)
        #y_classes = y_prob.argmax(axis=-1)
        print(y_prob, file)

    #im = Image.open(file)
    #im.show()

# entry point, run the example
run_example()
