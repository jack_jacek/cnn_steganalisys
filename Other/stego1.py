# https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html
# example of progressively loading images from file
# https://machinelearningmastery.com/how-to-load-large-datasets-from-directories-for-deep-learning-with-keras/
from keras import Sequential
from keras.applications import ResNet50
from keras.layers import Conv2D, Activation, MaxPooling2D, Flatten, Dense, Dropout
from keras.preprocessing.image import ImageDataGenerator
from numpy import unique
from tensorboard.plugins.hparams import keras

# create generator
datagen = ImageDataGenerator(rescale=1./255)
# rescale is a value by which we will multiply the data before any other processing.
# Our original images consist in RGB coefficients in the 0-255, but such values would be
# too high for our models to process (given a typical learning rate), so we target values
# between 0 and 1 instead by scaling with a 1/255. factor.


# prepare an iterators for each dataset
train_it = datagen.flow_from_directory('data/train/', class_mode='binary')
val_it = datagen.flow_from_directory('data/validation/', class_mode='binary')
test_it = datagen.flow_from_directory('data/test/', class_mode='binary')
predict_it = datagen.flow_from_directory('data/predict/', class_mode='binary')

labels = (train_it.class_indices)

# confirm the iterator works
batchX, batchy = train_it.next()
print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))


# determine the shape of the input images
in_shape = batchX.shape[1:]
# determine the number of classes
n_classes = len(unique(batchy))
print(in_shape, n_classes)

resnet_weights_paths = "resnet50_weights_tf_dim_ordering_tf_kernels.h5"

model = Sequential()
model.add(ResNet50(include_top = False, pooling = 'avg', weights = None, input_shape=(in_shape)))


model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])
batch_size = 32
model.fit_generator(
        train_it,
        steps_per_epoch=1000 // batch_size,
        epochs=2,
        validation_data=val_it,
        validation_steps=100 // batch_size)
model.save_weights('first_try.h5')  # always save your weights after training or during training
