# example of progressively loading images from file
# https://machinelearningmastery.com/how-to-load-large-datasets-from-directories-for-deep-learning-with-keras/

from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot
import matplotlib.pyplot as plt
from numpy import unique
from numpy import argmax
from tensorflow.keras.datasets.mnist import load_data
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPool2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dropout

# create generator
datagen = ImageDataGenerator(rescale=1./255)

# prepare an iterators for each dataset
train_it = datagen.flow_from_directory('data/train/', class_mode='binary')
val_it = datagen.flow_from_directory('data/validation/', class_mode='binary')
test_it = datagen.flow_from_directory('data/test/', class_mode='binary')
predict_it = datagen.flow_from_directory('data/predict/', class_mode='binary')

labels = (train_it.class_indices)


# confirm the iterator works
batchX, batchy = train_it.next()
print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))


# determine the shape of the input images
in_shape = batchX.shape[1:]

# determine the number of classes
n_classes = len(unique(batchy))
print(in_shape, n_classes)

# define model
model = Sequential()

model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=in_shape))
model.add(MaxPool2D((2, 2)))

model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
model.add(MaxPool2D((2, 2)))

model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
model.add(MaxPool2D((2, 2)))

model.add(Flatten())
model.add(Dense(64, activation='relu', kernel_initializer='he_uniform'))
model.add(Dense(2, activation='relu'))
model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['accuracy'])

# fit the model
history = model.fit(train_it, epochs=150, verbose=1)

# evaluate the model
loss, acc = model.evaluate(test_it, verbose=1)
print('Accuracy: %.3f' % acc)



# make a prediction
predictions = model.predict(predict_it)

print("Labels: ", labels)
for yhat in predictions:
    print('Predicted: class=%d' % argmax(yhat))

# list all data in history
print("Keys: ", history.history.keys())

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['loss'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
