# make a prediction for a new image.
import random
from keras.preprocessing.image import load_img, ImageDataGenerator
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import glob

# load and prepare the image
from networkx.algorithms.assortativity.tests.test_correlation import np


def load_image(filename):
    # load the image
    img = load_img(filename, target_size=(256, 256))

    # convert to array
    img_tensor = img_to_array(img)

    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.
    return img_tensor


# load an image and predict the class
def run_example():

    # load model
    model = load_model('data/epp_epoch_50_8008_SGD.h5')
    #print(model.summary())
    file_list =  glob.glob("data/f5/*.jpg")
    y = 100
    final = 0

    files_total = len(file_list)-1
    #print("number of files: ", files_total)
    sequence = [i for i in range(files_total)]



    # randomly shuffle the sequence
    random.shuffle(sequence)

    for x in range(y):
        file = file_list[sequence[x]]
        img = load_image(file)
        # predict the class
        result = model.predict_classes(img)
        #print(x+1, "- File: ", file[10:], ", Prediction: ", result[0])
        if (result[0]==1):
            final=final+1
    print(final/y)


# entry point, run the example
for z in range(10):
    run_example()
