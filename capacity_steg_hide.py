import os
import numpy as np

steg_hide_tool = "G:/Steghide/steghide"
image_org_path = "G:/ITB_Images/256x256/"
string_to_find = "capacity:"
capacity_array = []


def capacity():
    for filename in os.listdir(image_org_path):
        output = os.popen(steg_hide_tool+" info " + image_org_path + filename).read()
        value_pos = output.find(string_to_find)
        value_unit = output[value_pos+10:]

        unit = value_unit.find("Byte")
        if(unit>0):
            # Bytes / 1000
            value = float(value_unit[0:unit-1])/1000
        else:
            # KB
            value = float(value_unit[0:value_unit.find("KB")-1])

        capacity_array.append(value)
        print(">>> Output:", value, "KB")

def mean():
    import statistics
    print("Average capacity: ", statistics.mean(capacity_array))

def save_as_csv():
    # convert to NumPy array and save as csv file
    capacity_numpy_atray = np.array(capacity_array)
    np.savetxt("capacity.csv", capacity_numpy_atray, delimiter=",")

if __name__ == "__main__":
    capacity()
    mean()
    save_as_csv()
