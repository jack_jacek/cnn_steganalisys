# Pythono3 code to rename multiple
# files in a directory or folder

# importing os module
import os

path_name = "G:/ITB_Images/Test_Final256x256/"
# Function to rename multiple files
def main():
    i = 1

    jpg_name = "image_test_"
    for filename in os.listdir(path_name):
        dst = jpg_name+"0" + str(i) + ".jpg"
        src = path_name + filename
        dst = path_name + dst

        # rename() function will
        # rename all the files
        os.rename(src, dst)
        i += 1

# Driver Code
if __name__ == '__main__':

    # Calling main() function
    main()
